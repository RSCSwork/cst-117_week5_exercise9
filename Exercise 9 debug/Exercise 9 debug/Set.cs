﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Lydia's code - find the errors!

//Corrected by Rikk Shimizu
//debug project for exercise 9

/*
 * Some debugging techniques include, use of break points 
 * print lines, and toggling some parts of the code by changing numbers.
 * 
 * break points can be used to stop or skip over certain 
 * parts of the code which allow the user to see if that method
 * is causing a problem.
 * 
 * print lines always help to see what is going on and can be used
 * in most scenarios. 
 * 
 * Although something can be overlooked is understanding
 * the logic to get to where the code needs to be. This can allow 
 * someone to follow the code logic and alter anything they see is 
 * not correct to the end goal.
 */


namespace CST117_IC08_console
{
    class Set
    {
        private List<int> elements;

        public Set()
        {
            elements = new List<int>();
        }

        public bool addElement(int val)
        {
            if (containsElement(val)) return false;
            else
            {
                //Console.WriteLine(val); used for debugging. 
                elements.Add(val);
                return true;
            }
        }

        private bool containsElement(int val)
        {

            for (int i = 0; i < elements.Count; i++)
            {
                if (val == elements[i])
                    return true;
                /*
                 * The following lines force a return without checking the entire array for the value. 
                 * This means it only checks the current position for a duplicate. By removing it, you 
                 * can cycle through the rest of the for loop to check for duplicates.
                 */
                //else
                    //return false;
            }
            return false;
        }

        public override string ToString()
        {
            string str = "";
            foreach (int i in elements)
            {
                str += i + " ";
            }
            return str;
        }

        public void clearSet()
        {
            elements.Clear();
        }

        /*
         * This union function seems to just add the parts of set B to the end of Set A
         * This doesn't make much sense to just do that straight away since you also want to 
         * return the values of the original A. I've added a second parameter which will act 
         * as a storage unit for the unionized values... Set A will become untouched, and the return 
         * will send back the temporary holder.  I do not believe the base code here is sufficient
         * for unionizing and then also returning the original values that were in set A later. 
         * This is to stick with line 51 in Program.cs where it says the two original sets 
         * should be unchanged...
         */
        //public Set union(Set rhs)
        public Set union(Set rhs, Set temp)
        {
            /*
             * First thing to do now is to add the values from the current Set A,
             * to the Set temp...
             */
            for(int j = 0; j <this.elements.Count; j++)
            {
                temp.addElement(this.elements[j]);
            }//ends newly added for loop, used j instead of i, since the original code used i 

            /*
             * Now that the set temp has A's values, we can add the Set B values to it
             * we do not have to worry about clean up for A, and will not require additional 
             * methods. 
             */
            for (int i = 0; i < rhs.elements.Count; i++)
            {
                /*
                 * rather than use this.addElement which will alter Set A and require additional clean up.
                 * it is better to use the temp.addElement and add both A and B's values to the temp using the
                 * addElement method which will still weed out any duplicate values now that it is fixed.
                */
                //this.addElement(rhs.elements[i]);
                //Console.WriteLine(rhs.elements[i]); //Debug line....
                temp.addElement(rhs.elements[i]);
            }

            //this return sends back what was passed in, which is Set B, nothing was changed in B
            //The changes to Set A need to be returned. 
            //return rhs;
            return temp;
        }
    }
}
