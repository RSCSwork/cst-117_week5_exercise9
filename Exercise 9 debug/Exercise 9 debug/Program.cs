﻿//use for IC08
//Lydia's code
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Corrected by Rikk Shimizu
//debug project for exercise 9

namespace CST117_IC08_console
{
    class Program
    {
        static void Main(string[] args)
        {
            //make some sets
            Set A = new Set();
            Set B = new Set();

            /*There should be a temporary set that can store the origianl A data or B
             *data depending on thought process. Overall adding this Set temp, and a
             * a for loop to the union method which should take in two sets now, only 
             * adds 3 lines of code while utilizing the code that was given. 
             */
            Set temp = new Set();

            //put some stuff in the sets
            Random r = new Random();
            for (int i = 0; i < 10; i++)
            {
                A.addElement(r.Next(4));
                B.addElement(r.Next(12));
            }

            //display each set and the union
            Console.WriteLine("A: " + A);
            Console.WriteLine("B: " + B);

            /*
             * Union function does not work properly, needs to be changed. 
             * After looking at the union function, it makes more sense 
             * to use a temp Set and pass in two Sets. One that you want 
             * to unionize, and the second Set that will hold both Sets for 
             * the union. 
             */
            //Console.WriteLine("A union B: " + A.union(B));
            Console.WriteLine("A union B: " + A.union(B, temp));

            //display original sets (should be unchanged)
            Console.WriteLine("After union operation");
            Console.WriteLine("A: " + A);
            Console.WriteLine("B: " + B);

        }
    }
}

